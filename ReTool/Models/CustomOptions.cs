﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReTool
{
    public class CustomOptions
    {
        [Option('d', "directory", Group = "TargetType", HelpText = "目录，如'C:\\Temp'")]
        public string DicPath { get; set; }

        [Option('f', "file", Group = "TargetType", HelpText = "文件路径，如'C:\\Temp\\1.txt'")]
        public string FilePath { get; set; }


        [Option('s', "source", Required = true, HelpText = "原始字符串")]
        public string Source { get; set; }

        [Option('t', "target", Required = true, HelpText = "替换字符串")]
        public string Target { get; set; }

        [Option('m', "mode", Required = true, HelpText = "替换模式： \r\n 0-同时替换目录名、文件名、文件内容(需配合 -d 参数) \r\n 1-仅替换目录名 \r\n 2-仅替换文件名 \r\n 3-仅替换文件内容")]
        public ushort Mode { get; set; }


        [Option('e', "extension", Required = false, HelpText = "要排除的后缀名，如：dll,exe")]
        public string Extensions { get; set; }

        [Option('w', "watcher", Required = false, HelpText = "开启文件监控")]
        public bool Watcher { get; set; }

        public static readonly ushort Max = 3;
        public static readonly ushort Min = 0;
    }
}
