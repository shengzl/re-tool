﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReTool
{
    public static partial class ReHandler
    {
        private static void Info(string msg, bool onlyPrint = true)
        {
            Base(ConsoleColor.Green, msg, onlyPrint);
        }
        private static void Warn(string msg, bool onlyPrint = true)
        {
            Base(ConsoleColor.Yellow, msg, onlyPrint);
        }
        private static void Error(string msg, bool onlyPrint = false)
        {
            Base(ConsoleColor.Red, msg, onlyPrint);
        }
        private static void Base(ConsoleColor color, string msg, bool onlyPrint = false)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(msg);
            Console.ForegroundColor = ConsoleColor.Gray;
            if (!onlyPrint)
            {
                throw new Exception();
            }
        }
    }
}
