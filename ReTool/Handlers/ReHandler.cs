﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReTool
{
    /// <summary>
    /// 处理
    /// </summary>
    public static partial class ReHandler
    {
        private static CustomOptions _option;

        /// <summary>
        /// 入口
        /// </summary>
        /// <param name="options"></param>
        public static void Exec(CustomOptions options)
        {
            if (string.IsNullOrEmpty(options.DicPath) && string.IsNullOrEmpty(options.FilePath))
            {
                Error("目录和文件仅可以选择一项");
            }
            if (string.IsNullOrEmpty(options.Source) || string.IsNullOrEmpty(options.Target))
            {
                Error("原始、替换的内容不能为空，请使用 -s 指定原始内容 -t 指定替换内容");
            }
            if ((options.Mode > CustomOptions.Max) || (options.Mode < CustomOptions.Min))
            {
                Error("-m 参数不满足上下限");
            }

            _option = options;

            //开启文件监控
            if (_option.Watcher)
            {
                Task.Run(() =>
                {
                    BeginMonitor();
                });
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }
            
            switch (_option.Mode)
            {
                case 0:
                    ReAll();
                    break;
                case 1:
                    ReDirectory(_option.DicPath);
                    break;
                case 2:
                    ReFileName(_option.FilePath);
                    break;
                case 3:
                    ReFileContent(_option.FilePath);
                    break;
                default:
                    break;
            }
        }
    }
}
