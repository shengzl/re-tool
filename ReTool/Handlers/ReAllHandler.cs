﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ReTool
{
    public static partial class ReHandler
    {
        /// <summary>
        /// 替换全部
        /// </summary>
        public static void ReAll()
        {
            DirectoryInfo di = new DirectoryInfo(_option.DicPath);
            if (!di.Exists)
            {
                Error($"目录 {_option.DicPath} 不存在", true);
                return;
            }

            var files = di.GetFiles();
            _ReAllFile(files);

            var reDi = new DirectoryInfo(ReDirectory(di.FullName));
            var directories = reDi.GetDirectories();
            _ReAllDirectory(directories);
        }

        private static void _ReAllDirectory(DirectoryInfo[] directories)
        {
            foreach (var item in directories)
            {
                var files = item.GetFiles();
                _ReAllFile(files);

                var reDi = new DirectoryInfo(ReDirectory(item.FullName));
                var subDirs = reDi.GetDirectories();
                _ReAllDirectory(subDirs);
            }
        }
        private static void _ReAllFile(FileInfo[] files)
        {
            foreach (var item in files)
            {
                ReFileContent(ReFileName(item.FullName));
            }
        }
    }

}
