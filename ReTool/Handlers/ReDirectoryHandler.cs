﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ReTool
{
    public static partial class ReHandler
    {
        /// <summary>
        /// 修改目录名 level 0
        /// </summary>
        /// <param name="dirPath">原始目录绝对路径</param>
        /// <returns>新目录的绝对路径</returns>
        public static string ReDirectory(string dirPath)
        {
            DirectoryInfo di = new DirectoryInfo(dirPath);
            if (!di.Exists)
            {
                Error($"目录 {dirPath} 不存在", true);
                return string.Empty;
            }

            var destDirName = Path.Combine(di.Parent.FullName, di.Name.Replace(_option.Source, _option.Target));
            if (dirPath.Equals(destDirName))
            {
                return dirPath;
            }
            di.MoveTo(destDirName);

            return destDirName;
        }
    }
}
