﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReTool
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            var result = CommandLine.Parser.Default.ParseArguments<CustomOptions>(args)
                .MapResult(
                    (opts) => RunOptionsAndReturnExitCode(opts), //in case parser sucess
                    errs => HandleParseError(errs) //in  case parser fail
                );
            Console.WriteLine("Return code= {0}", result);
        }

        static int RunOptionsAndReturnExitCode(CustomOptions o)
        {
            try
            {
                ReHandler.Exec(o);
            }
            catch (Exception ex){; }
            Console.WriteLine("Success");
            var exitCode = 0;
            return exitCode;
        }

        static int HandleParseError(IEnumerable<Error> errs)
        {
            var result = -2;
            Console.WriteLine("errors {0}", errs.Count());
            if (errs.Any(x => x is HelpRequestedError || x is VersionRequestedError))
                result = -1;
            Console.WriteLine("Exit code {0}", result);
            return result;
        }
    }
}
