# ReTool

#### 介绍
目录、文件名、内容批量替换控制台程序

#### 使用说明
- 注意：使用时若项目中含有 .git 目录，请先删除该目录

- 可以编译后直接使用 <code> ./ReTool.exe --help </code>查看参数                

- 或者直接查看定义文件中支持的参数
    > [定义文件](ReTool/Models/CustomOptions.cs)

- 常用参数组合
    ```bash

    # 将 <code>F:\\Demo</code> 目录下所有文件夹名、文件名、文件内容中的 <code> CoreDemo </code> 替换为 <code>DingMachine</code>
    # 同时忽略后缀为 <code>dll,exe,pdb</code> 的文件；同时开启文件修改监控
    ./ReTool.exe -s "CoreDemo" -t "DingMachine" -d "F:\\Demo" -m 0 -w -e "dll,exe,pdb"

    #仅替换文件夹名(单文件夹)， <code> CoreDemo </code> 替换为 <code>DingMachine</code>，不开启文件监控
    ./ReTool.exe -s "CoreDemo" -t "DingMachine" -d "F:\\Demo" -m 1

    #仅替换文件名(单文件)， <code> CoreDemo </code> 替换为 <code>DingMachine</code>，不开启文件监控
    ./ReTool.exe -s "CoreDemo" -t "DingMachine" -f "F:\\Demo\\1.txt" -m 2

    #仅替换文件内容(单文件)， <code> CoreDemo </code> 替换为 <code>DingMachine</code>，不开启文件监控
    ./ReTool.exe -s "CoreDemo" -t "DingMachine" -d "F:\\Demo\\1.txt" -m 3

    ```

#### 说明
> 基于本项目代码可以很容易实现替换单文件夹的所有文件的其他功能，但因为作者本人仅用以批量修改项目内容，因此没有继续扩展，有要扩展的可以提merge~

#### 联系作者
> [作者小站](https://www.shengzilong.com)